<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	// return view('welcome');
    return redirect(route('categories.index'));
});

Auth::routes();
Route::put('tickets/complete/{ticket}', 'TicketController@complete')->name('tickets.complete');
Route::put('tickets/reject/{ticket}', 'TicketController@reject')->name('tickets.reject');
// Route::get('/workers/{worker}','WorkerController@index')->name
Route::get('/home', 'WorkerController@index')->name('home');
Route::resource('workers', 'WorkerController');
Route::resource('categories', 'CategoryController');
Route::resource('tickets', 'TicketController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
