@extends('layouts.app')

@section('content')
<div class="container">
	{{-- <div class="row">
		<div class="col-12 col-md-8 mx-auto">
			<div class="row">
				<h2>My Pending Requests</h2>
				<table class="table table-dark">
					<thead>
						<tr>
							<th scope="col">Product Line</th>
							<th scope="col">Ref. No.</th>
							<th scope="col">Borrow Date</th>
							<th scope="col">Return Date</th>
							<th scope="col">Request Date</th>
							<th scope="col">Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach($tickets as $ticket)
						<tr>
							<td>{{$ticket->id}}</td>
							<td>TEST</td>
							<td>TEST</td>
							<td>TEST</td>
							<td>TEST</td>
							<td>
								<button data-id="36" class="btn btn-warning cancelBtns">Cancel</button>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
				<h2>My Approved Requests</h2>
				<table class="table table-dark">
					<thead>
						<tr>
							<th scope="col">Ref. No.</th>
							<th scope="col">Asset Serial #</th>
							<th scope="col">Borrow Date</th>
							<th scope="col">Return Date</th>
							<th scope="col">Approved On</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
				<h2>My Completed Requests</h2>
				<table class="table table-dark">
					<thead>
						<tr>
							<th scope="col">Ref. No.</th>
							<th scope="col">Asset Serial #</th>
							<th scope="col">Borrow Date</th>
							<th scope="col">Return Date</th>
							<th scope="col">Status</th>
							<th scope="col">Completed On</th>
						</tr>
					</thead>
					<tbody id="completedTransactions">
						<tr>
							<td>TEST</td>
							<td>TEST</td>
							<td>TEST</td>
							<td>TEST</td>
							<td>TEST</td>
							<td>TEST</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div> --}}
	<div class="row">
		<div class="col-lg-8 offset-lg-2">
			<h2 class="text-white"> <i class="fas fa-ticket-alt"></i> Ticket Pending</h2>

			<table class="table table-dark">
				<thead>
					<tr>
						<th scope="col">User</th>
						<th scope="col">Category</th>
						<th scope="col">Worker</th>
						<th scope="col">Duration</th>
						<th scope="col">Confirmation</th>
					</tr>
				</thead>

				<tbody>
					@foreach($tickets as $ticket)
					@if($ticket->status->name == 'pending')
					<tr>
						<td>{{$ticket->user->name}}</td>
						
						@foreach($ticket->workers as $ticketworkers)
						<td>{{$ticketworkers->category->name}}</td>
						<td>{{$ticketworkers->name}}</td>
						<td>{{$ticketworkers->pivot->duration}}</td>
						@endforeach
						<td>
							@if(Auth::user()->role_id==1)
							<form action="{{route('tickets.update',['ticket' => $ticket->id]) }}" method="post">
								@csrf
								@method('PUT')
								
								<input type="hidden" name="status" value="2">
								<button class="btn btn-success approveBtns">Approve</button>
							</form>
							<form action="{{route('tickets.reject',['ticket' => $ticket->id]) }}" method="post">
								@csrf
								@method('PUT')
								<input type="hidden" name="status" value="4">
								<button class="btn btn-danger rejectBtns">Reject</button>
							</form>
							@else
							<form action="{{route('tickets.destroy',['ticket' => $ticket->id]) }}" method="post">
								@csrf
								@method('DELETE')
								<button class="btn btn-warning rejectBtns">Cancel</button>
							</form>
							@endif
						</td>
					</tr>
					@endif
					@endforeach
				</tbody>                    
			</table>
			<hr>
			<h2 class="text-white"> <i class="fas fa-check-square"></i> Approved Transactions</h2>
			<table class="table table-dark">
				<thead>
					<tr>
						<th scope="col">User</th>
						<th scope="col">Reference Number</th>
						<th scope="col">Worker</th>
						<th scope="col">Duration</th>
						<th scope="col">Approved On</th>
						@if(Auth::user()->role_id==1)
						<th scope="col">Actions</th>
						@endif
					</tr>
				</thead>

				<tbody id="approvedTransactions">
					@foreach($tickets as $ticket)
					@if($ticket->status->name == 'approved')
					<tr>
						<td>{{$ticket->user->name}}</td>
						<td>{{($ticket->reference_number) }}</td>
						@foreach($ticket->workers as $ticketworkers)
						<td>{{$ticketworkers->name}}</td>
						<td>{{$ticketworkers->pivot->duration}}</td>
						@endforeach
						<td>{{$ticket->updated_at}}</td>
						@if(Auth::check())
						@if(Auth::user()->role_id==1)
						<td>
							<form action="{{route('tickets.complete',['ticket' => $ticket->id]) }}" method="post">
								@csrf
								@method('PUT')
								<input type="hidden" name="status" value="3">
								<button class="btn btn-success returnBtns">Hours Done</button>
							</form>
						</td>
						@endif
						@endif
					</tr>
					@endif
					@endforeach
				</tbody>                    
			</table>

			<hr>

			<h2 class="text-white"> <i class="fas fa-clipboard-list"></i> Completed Transactions</h2>
			<table class="table table-dark">
				<thead>
					<tr>
						<th scope="col">User</th>
						<th scope="col">Reference Number</th>
						<th scope="col">Worker</th>
						<th scope="col">Duration</th>
						<th scope="col">Status</th>
						<th scope="col">Completed On</th>
					</tr>
				</thead>

				<tbody id="completedTransactions">
					@foreach($tickets as $ticket)
					@if($ticket->status->name == 'completed' || $ticket->status->name == 'rejected')
					<tr>
						<td>{{$ticket->user->name}}</td>
						<td>{{($ticket->reference_number) }}</td>
						@foreach($ticket->workers as $ticketworkers)
						<td>{{$ticketworkers->name}}</td>
						<td>{{$ticketworkers->pivot->duration}}</td>
						@endforeach
						<td>{{$ticket->status->name}}</td>
						<td>{{$ticket->updated_at}}</td>
					</tr>
					@endif
					@endforeach
				</tbody>                    
			</table>
		</div>
	</div>

</div>

</div>
@endsection