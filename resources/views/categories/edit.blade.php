@extends('layouts.app')

@section('content')

<div class="container">
	<div class="row">
		<div class="col-12 col-md-8 mx-auto">
			<h3 class="text-center text-white">
				Edit this Category
			</h3>
			<hr>
			@if($errors->has('category_name'))
			<div class="alert alert-warning text-center m-0">
				<p>Name needs to be unique!</p>
			</div>
			@endif
			@if(Session::has('update_success'))
				<div class="alert alert-success text-center">
					{{ Session::get('update_success')}}
				</div>
				@endif
			{{-- @if(Session::has('update_failed'))
			<div class="alert alert-danger text-center">
				{{ Session::get('update_failed')}}
			</div>
			@elseif(Session::has('update_success'))
			<div class="alert alert-success text-center">
				{{ Session::get('update_success')}}
			</div>
			@endif --}}
			<form method="post" action="{{ route('categories.update', ['category' => $category->id])}}" enctype="multipart/form-data">
				@csrf
				@method('PUT')
				{{-- Input for name --}}
				<div class="form-group">
					<label for="name" class="text-white">Category name:</label>
					<input type="text" name="category_name" class="form-control" id="name" value="{{$category->name}}">
				</div>

				@if($errors->has('name'))
				<div class="alert alert-danger">
					<p>All Fields Required</p>
				</div>
				@endif


				{{-- Input for description --}}
				<div class="form-group">
					<label for="description" class="text-white">Category description</label>
					<textarea name="category_description" class="form-control" id="description" cols="30" rows ="10">{{$category->description}}
					</textarea>
				</div>

				@if($errors->has('description'))
				<div class="alert alert-danger">
					<p>All Fields Required</p>
				</div>
				@endif

				

				<button class="btn btn-primary w-100">Edit category</button>
			</form>
		</div>
	</div>		
</div>

@endsection