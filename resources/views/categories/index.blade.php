@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-12">
			<div class="jumbotron bg-secondary">
				<h1 class="text-center text-white">
					Doing it for the money not love.
				</h1>
			</div>
		</div>
	</div>
	@if(Session::has('destroy_message'))
	<div class="alert alert-warning text-center">
		{{ Session::get('destroy_message')}}
	</div>
	@endif
	<div class="row">
		<div class="col-12 col-md-12 mx-auto">
			<div class="row">
				@foreach($categories as $category)
				<div class="col-12 col-md-4 mx-auto">
					<div class="card text-center m-2 bg-secondary ">
						<div class="card-header"  id="headingOne">
							<h5 class="mb-0">
								{{-- <button class="btn btn-link" data-toggle="" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"> --}}
									<p class="text-dark bg-white p-1 m-0">{{$category->name}}</p>
								{{-- </button> --}}
							</h5>
							<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
								<div class="card-body text-white match">
									{{$category->description}}
								</div>
							</div>
						</div>
						{{-- Buttons --}}
						<div class="card-footer">
							<a href="{{ route('workers.index', ['category'=>$category->id])}}" class="btn btn-success w-100"> View</a>
							@if(Auth::check())
							@if(Auth::user()->role_id==1)
							<a href="{{ route('categories.edit', ['category' => $category->id] )}}" class="btn btn-primary w-100"> Edit</a>
							<form action="{{route('categories.destroy', ['category' => $category->id])}}" method="post">
								@csrf
								@method('DELETE')
								<button class="btn btn-danger w-100 ">Delete</button>
							</form>
							@endif
							@endif	
						</div>
						{{-- end buttons a--}}
					</div>
				</div>
				@endforeach	
			</div>
		</div>
	</div> {{-- end row --}}
</div>

@endsection


{{-- @foreach($categories as $category) --}}
{{-- <div class="col-12 col-md-12 m-2"> --}}
	{{-- card start --}}
	{{-- <div class="card text-center bg-secondary"> --}}
		{{-- body --}}
		{{-- <div class="card-body bg-dark text-white"> --}}
			{{-- <h2>{{$category->name}}</h2> --}}
			{{-- <p class="card-text	"> --}}
				{{-- {{$category->description}} --}}
			{{-- </p> --}}
		{{-- </div> --}}
		{{-- footer --}}
		{{-- <div class="card-foot bg-dark"> --}}

			{{-- @can('isAdmin') --}}
			{{-- <a href="{{ route('categories.edit', ['category' => $category->id] )}}" class="btn btn-primary w-10 float-right"> Edit</a> --}}
			{{-- <form action="{{route('categories.destroy', ['category' => $category->id])}}" method="post"> --}}
				{{-- @csrf --}}
				{{-- @method('DELETE') --}}
				{{-- <button class="btn btn-danger w-10 float-right">Delete</button> --}}
			{{-- </form> --}}
			{{-- @endcan --}}
		{{-- </div> --}}
	{{-- </div> --}}
	{{-- end card --}}
{{-- </div> --}}
{{-- @endforeach --}}