@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-12">
			<div class="jumbotron bg-secondary">
				<h1 class="text-center text-white">
					{{-- Hire us! Hire us! --}}

					@if(count($workers))
					@foreach($workers as $worker)
					@endforeach

					@if($worker->category_id == 1)
					Strange jobs for your strange needs
					@elseif($worker->category_id == 2)
					On a cold rainy day I held your hand... and then time was up
					@elseif($worker->category_id == 3)
					Cooking the meals that your loved ones won't
					@elseif($worker->category_id == 4)
					Hire us or now or your friend will!
					@else
					The customer is mostly wrong
					@endif

					@else
					No Worker for this
					@endif
					
					{{-- {{dd($workers->category_id)}} --}}


					

					
				</h1>
			</div>
			
		</div>
	</div>
	@if(Session::has('destroy_message'))
	<div class="alert alert-warning text-center">
		{{ Session::get('destroy_message')}}
	</div>
	@endif
	<div class="row">
		<div class="col-12 col-md-10 ">
			<div class="row">
				{{-- {{dd($workers)}} --}}
				{{-- @foreach($category as $cat) --}}
				{{-- {{dd($cat->id)}} --}}
				
				@foreach($workers as $worker)
				{{-- {{dd($worker->category_id)}} --}}
				
				{{-- @if($worker->category_id == $cat->id) --}}

				<div class="col-12 col-md-4 col-lg-3 ">
					{{-- card start --}}
					<div class="card text-center bg-secondary match">
						<img src="{{url('/public/'.$worker->image)}}" class="card-img-top">
						{{-- body --}}
						<div class="card-body bg-dark text-white">
							<h2>{{$worker->name}}</h2>
							<p class="card-text">
								<strong>
									&#8369; {{$worker->price}}
								</strong>
							</p>
							<p class="card-text ">
								{{-- {{$worker->availability_id}} --}}
								@if($worker->availability_id == 1)
								<p class="text-success">Available</p>
								@else
								<p class="text-danger">Unavailable</p>
								@endif
							</p>
							<p class="card-text	bg-secondary text-light ">
								{{$worker->introduction}}
							</p>
						</div>
						{{-- footer --}}
						<div class="card-foot bg-dark">
							@if(Auth::check())
							@if(Auth::user()->role_id==1)
							<a href="{{ route('workers.show', ['worker' => $worker->id] )}}" class="w-100 btn btn-info my-1"> View</a>
							{{-- @can('isAdmin') --}}
							<a href="{{ route('workers.edit', ['worker' => $worker->id] )}}" class="w-100 btn btn-primary my-1"> Edit</a>
							<form action="{{route('workers.destroy', ['worker' => $worker->id])}}" method="post">
								@csrf
								@method('DELETE')
								<button class="btn btn-danger w-100 my-1">Delete</button>
							</form>
							@endif
							
							{{-- {{dd($worker->availability_id)}} --}}
							@if($worker->availability_id == 1 && Auth::user()->role_id!=1)
							<form action="{{route('tickets.store')}}" method="post">
								<div class="form-group my-1">
									@csrf
									<input type="hidden" name="id" value="{{$worker->id}}">
									<h6 class="text-light m-0"> Duration (Hours)</h6>
									<input class="text-center text-secondary" type="number" name="duration" value="1" min="1">
								</div>
								<button type="submit" class="btn btn-success w-100"> Request
								</button>
							</form>
							@endif
							@endif
							{{-- @endcan --}}
						</div>
					</div>
					{{-- end card --}}
				</div>
			{{-- 	@else
				<div class="jumbotron">
					<h1 class="text-center text-dark">
						No one.
					</h1>
				</div> --}}
				{{-- @endif --}}
				@endforeach
				{{-- @endforeach --}}
			</div>
		</div>
	</div> {{-- end row --}}
</div>

@endsection