@extends('layouts.app')

@section('content')

<div class="container">
	<div class="row">
		<div class="col-12 col-md-8 mx-auto">
			<h3 class="text-center text-white">
				Hire a Worker
			</h3>
			<hr>
			@if(Session::has('update_failed'))
			<div class="alert alert-danger text-center">
				{{ Session::get('update_failed')}}
			</div>
			@elseif(Session::has('update_success'))
			<div class="alert alert-success text-center">
				{{ Session::get('update_success')}}
			</div>
			@endif
			<form method="post" action="{{ route('workers.update', ['worker' => $worker->id])}}" enctype="multipart/form-data">
				@csrf
				@method('PUT')
				{{-- Input for name --}}
				<div class="form-group">
					<label for="name" class="text-white">Worker name:</label>
					<input type="text" name="name" class="form-control" id="name" value="{{$worker->name}}">
				</div>

				@if($errors->has('name'))
				<div class="alert alert-danger">
					<p>All Fields Required</p>
				</div>
				@endif

				{{-- Input for price --}}
				<div class="form-group">
					<label for="price" class="text-white">Hiring price:</label>
					<input type="number" name="price" class="form-control" id="price" value="{{$worker->price}}">
				</div>

				@if($errors->has('price'))
				<div class="alert alert-danger">
					<p>All Fields Required</p>
				</div>
				@endif

				{{-- Input for price --}}
				<div class="form-group">
					<label for="price" class="text-white">Serial Number:</label>
					<input type="number" name="serial_number" class="form-control" id="serial_number" value="{{$worker->serial_number}}">
				</div>

				@if($errors->has('serial_number'))
				<div class="alert alert-danger">
					<p>All Fields Required</p>
				</div>
				@endif

				{{-- Input category --}}
				<div class="form-group">
					<label for="category" class="text-white">Worker Category</label>
					<select class="form-control" id="category" name="category">
						@foreach($categories as $category)
						<option 
						value="{{$category->id}}"
						@if($worker->category_id == $category->id)
						selected
						@endif
						>
						{{$category->name}}
					</option>
					@endforeach
				</select>
			</div>

			@if($errors->has('category'))
			<div class="alert alert-danger">
				<p>All Fields Required</p>
			</div>
			@endif

			{{-- Input for image --}}
			<div class="form-group">
				<label for="image" class="text-white">Worker Image:</label>
				<input type="file" name="image" class="form-control-file text-white" id="image">
			</div>

			@if($errors->has('image'))
			<div class="alert alert-danger">
				<p>All Fields Required</p>
			</div>
			@endif

			{{-- Input for introduction --}}
			<div class="form-group">
				<label for="introduction" class="text-white">Worker introduction</label>
				<textarea name="introduction" class="form-control" id="introduction" cols="30" rows ="10">{{$worker->introduction}}</textarea>
			</div>

			@if($errors->has('introduction'))
			<div class="alert alert-danger">
				<p>All Fields Required</p>
			</div>
			@endif

			<button class="btn btn-primary w-100">Edit Worker</button>

		</form>
	</div>
</div>		
</div>

@endsection