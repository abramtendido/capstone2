@extends('layouts.app')

@section('content')

	<div class="container">
		<div class="row">
			<div class="col-12 col-md-8 mx-auto text-dark">
				<h3 class="text-center text-white">
					Hire a Worker
				</h3>
				<hr>
				<form method="post" action="{{ route('workers.store')}}" enctype="multipart/form-data">
					@csrf
					{{-- Input for name --}}
					<div class="form-group">
						<label for="name" class="text-white">Worker name:</label>
						<input type="text" name="name" class="form-control" id="name">
					</div>

					@if($errors->has('name'))
						<div class="alert alert-danger">
							<p>All Fields Required</p>
						</div>
					@endif
										
					{{-- Input for price --}}
					<div class="form-group">
						<label for="price" class="text-white">Hiring price:</label>
						<input type="number" name="price" class="form-control" id="price">
					</div>

					@if($errors->has('price'))
						<div class="alert alert-danger">
							<p>All Fields Required</p>
						</div>
					@endif

					{{-- Input category --}}
					<div class="form-group">
						<label for="category" class="text-white">Worker Category</label>
						<select class="form-control" id="category" name="category">
							@foreach($categories as $category)
							<option value="{{$category->id}}">{{$category->name}}</option>
							{{-- <option value="2">Fighting</option>
							<option value="3">Ghost</option>
							<option value="4">Fire</option>
							<option value="5">Psychic</option> --}}
							@endforeach
						</select>
					</div>

					@if($errors->has('category'))
						<div class="alert alert-danger">
							<p>All Fields Required</p>
						</div>
					@endif

					{{-- Input for introduction --}}
					<div class="form-group">
						<label for="introduction" class="text-white">Worker introduction</label>
						<textarea name="introduction" class="form-control" id="introduction" cols="30" rows ="10"></textarea>
					</div>

					@if($errors->has('introduction'))
						<div class="alert alert-danger">
							<p>All Fields Required</p>
						</div>
					@endif

					{{-- Input for introduction --}}
					<div class="form-group">
						<label for="serial_number" class="text-white">Worker serial number</label>
						<input type="number" name="serial_number" class="form-control" id="serial_number">
					</div>

					@if($errors->has('serial_number'))
						<div class="alert alert-danger">
							<p>All Fields Required</p>
						</div>
					@endif

					{{-- Input for image --}}
					<div class="form-group">
						<label for="image" class="text-white">Worker Image:</label>
						<input type="file" name="image" class="form-control-file text-white id="image">
					</div>

					@if($errors->has('image'))
						<div class="alert alert-danger">
							<p>All Fields Required</p>
						</div>
					@endif

					<button class="btn btn-primary w-100">Add Worker</button>
				</form>
			</div>

			<div class="col-12 col-md-2 mx-auto">
				@if($errors->has('add_category'))
					<div class="alert alert-danger">
						<p>!ERROR!</p>
					</div>
				@elseif(Session::has('category_message'))
					<div class="alert alert-success">
						{{ Session::get ('category_message') }}
					</div>

				@endif

				<form method="post" action=" {{route('categories.store')}}">
					@csrf
					<div class="form-group">
					<label for="add-category" class="text-white"> Category Name:</label>
					<input type="text" name="category_name" id="category_name" class="form-control">
					<label for="add-category" class="text-white"> Category Description:</label>
					<textarea type="text" name="category_description" id="category_description" class="form-control" cols="30" rows ="10"></textarea>
					</div>

				<button class="btn btn-secondary w-100">Add Category</button>
				</form>
			</div>
		</div>

	</div>


@endsection