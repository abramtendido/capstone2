@extends('layouts.app')


@section('content')
<div class="container">
	<div class="row">
		<div class="col-12 col-md-8 mx-auto">
			<h3 class="text-center text-light">
				See our Workers!
			</h3>
			<hr>
			<div class="card text-center bg-secondary">
				<img src="{{url('/public/'.$worker->image)}}" class="card-img-top">
				<div class="card-body bg-dark text-white">
					<h2>{{$worker->name}}</h2>
					<p class="card-text">
						<strong>
							&#8369; {{$worker->price}}
						</strong>
					</p>
					<p class="card-text	">
						{{$worker->introduction}}
					</p>
				</div>
				<div class="card-foot bg-dark">
					
					<a href="{{ route('workers.create') }}" class="w-100 btn btn-info my-1"> Add More</a>
					{{-- @can('isAdmin') --}}
					<a href="{{ route('workers.edit', ['worker' => $worker->id] )}}" class="w-100 btn btn-primary my-1"> Edit</a>
					<form action="{{route('workers.destroy', ['worker' => $worker->id])}}" method="POST">
						@csrf
						@method('DELETE')
						<button class="btn btn-danger w-100 my-1">Delete</button>
					</form>
					{{-- @endcan --}}
				</div>
			</div>
		</div>
	</div>
</div>

@endsection