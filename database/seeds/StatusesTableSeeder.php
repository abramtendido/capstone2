<?php

use Illuminate\Database\Seeder;

class StatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('statuses')->insert([
        	'name' => 'pending'
        ]);

        DB::table('statuses')->insert([
            'name' => 'approved'
        ]);

        DB::table('statuses')->insert([
        	'name' => 'completed'
        ]);

        DB::table('statuses')->insert([
        	'name' => 'rejected'
        ]);
    }
}
