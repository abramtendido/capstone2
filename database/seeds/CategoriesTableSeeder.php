<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('categories')->insert([
        	'name' => 'Odd Jobs',
        	'description' => 'Strange Jobs Here'
        ]);

        DB::table('categories')->insert([
        	'name' => 'Acting',
        	'description' => 'Will act as someone else just for you'
        ]);

        DB::table('categories')->insert([
        	'name' => 'Cooking',
        	'description' => 'Cooking that meal that you will never cook yourself'
        ]);

        DB::table('categories')->insert([
        	'name' => 'Pranks',
        	'description' => 'Prank me baby one more time'
        ]);
    }
}
