<?php

use Illuminate\Database\Seeder;

class AvailabiltyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('availabilities')->insert([
        	'name' => 'available'
        ]);

        DB::table('availabilities')->insert([
        	'name' => 'unavailable'
        ]);
    }
}
