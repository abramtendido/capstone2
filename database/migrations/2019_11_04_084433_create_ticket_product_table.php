<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_product', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->float('duration');

            // product id
            $table->unsignedBigInteger('worker_id');
            $table->foreign('worker_id')
            ->references('id')
            ->on('workers')
            ->onDelete('restrict')
            ->onUpdate('cascade');

            // transactions id
            $table->unsignedBigInteger('ticket_id');
            $table->foreign('ticket_id')
            ->references('id')
            ->on('tickets')
            ->onDelete('restrict')
            ->onUpdate('cascade');
            
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket__products');
    }
}
