<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->float('price', 15, 2);
            $table->longtext('introduction');
            $table->string('serial_number');
            $table->string('image');
            $table->unsignedBigInteger('category_id')->nullable(true);
            $table->foreign('category_id')
            ->references('id')->on('categories')
            ->onDelete('restrict')
            ->onUpdate('cascade');

            $table->unsignedBigInteger('availability_id')->nullable(true)->default(1);
            $table->foreign('availability_id')
            ->references('id')->on('availabilities')
            ->onDelete('restrict')
            ->onUpdate('cascade');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('workers');
    }
}
