<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ticket extends Model
{
    use SoftDeletes;
    public function status()
    {
        return $this->belongsTo('App\Status');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function workers()
    {
        return $this->belongsToMany('App\Worker', 'ticket_product')
            ->withPivot('duration')
            ->withTimestamps();
    }
}
