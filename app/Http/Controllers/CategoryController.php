<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        return view('categories.index')->with('categories', $categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Category $category)
    {
        $request->validate([
            'category_name' => 'required|unique:categories,name',
            'category_description' => 'required|string'
        ]);

        $name = $request->input('category_name');
        $description = $request->input('category_description');


        $new_category = new Category;
        $new_category->name = $name;
        $new_category->description = $description;

        $new_category->save(); //save to database
        // dd($new_category);

        $request->session()->flash('category_message', 'Category successfully added!');
        return redirect(route('categories.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        return view('categories.show')->with('category', $category);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $this->authorize('update', $category);
        return view('categories.edit')
        ->with('category', $category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {   

        $this->authorize('update', $category);
        // dd($category);
        $request->validate([
            'category_name' => 'required|unique:categories,name',
            'category_description' => 'required|string'
        ]);

        // $request->session()->flash('update_failed','Whoops! Nothing Changed');

        $category->name = $request->input('category_name');
        $category->description = $request->input('category_description');
        $category->save();   

        $request->session()->flash('update_success', 'Updated Category!');
        return redirect( route('categories.edit', ['category' => $category->id]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        //just a comment
        $category->delete();
        return redirect(route('categories.index'))
        ->with('destroy_message', 'Category Deleted');
    }
}
