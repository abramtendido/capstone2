<?php

namespace App\Http\Controllers;

use App\Ticket;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Str;
use Session;
use App\Worker;
use App\Status;
use App\User;

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user, Ticket $ticket)
    {
        $this->authorize('view', $ticket);
        // $this->authorize('view', $user);

        if(Auth::user()->role_id==1)
        {
            $statuses = Status::all();
            $tickets = Ticket::all();
            
        }
        else
        {
            $statuses = Status::all()->where('user_id', Auth::user()->id);
            $tickets = Ticket::all()->where('user_id', Auth::user()->id);
        }

        return view('tickets.index')
        ->with('tickets', $tickets)
        ->with('statuses', $statuses);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         // Flow
            // 1. create an entry in tickets table with he following attributes
                // reference_number 
                // status_id
                // user_id
        $ticket = new Ticket;
        // $ticket_number = Auth::user()->id . Str::random(5) . time();
        // dd(Auth::user());
        $reference_number = Auth::user()->name . Str::random(5). '1z' . time() . '2D\/mb';
        $user_id = Auth::user()->id;
        // $user_id = 1;

        $ticket->reference_number = $reference_number;
        $ticket->user_id = $user_id;
        $ticket->save();  
        
        $ticket->workers()->attach(
            $request->input('id'), 
            [
                'duration' => $request->input('duration'),
                'worker_id' => $request->input('id'),
                'ticket_id' => $ticket->id
            ]
        );
        // dd($ticket->workers);

        return redirect(route('tickets.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function show(Ticket $ticket)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function edit(Ticket $ticket)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ticket $ticket, Worker $worker)
    {
        // dd($ticket->workers);

        $ticket->status_id = 2;
        $ticket->save();
        // dd($ticket->workers[0]->id);
        // foreach ($ticket->workers as $ticketworker) {
        //     // dd($ticketworkers);
        //     $ticketworker->update([
        //         'availability_id' => 2
        //     ]);
        //     // dd($ticketworkers->availability_id);
        // }


        // $worker->availability_id=2;
            // $worker->save();
        $ticket->workers[0]->where('id', $ticket->workers[0]->id)->update([
            'availability_id' => 2
        ]);

        return redirect(route('tickets.index'))
        ->with('updated_ticket', $ticket->id);
    }

    public function complete(Request $request, Ticket $ticket, Worker $worker)
    {
        $ticket->status_id = 3;
        $ticket->save();
        $ticket->workers[0]->where('id', $ticket->workers[0]->id)->update([
            'availability_id' => 1
        ]);
        // $ticket->workers[0]->availability_id
        // $ticket->workers[0]->save();

        // foreach ($ticket->workers as $ticketworker) {
        //     // dd($ticketworkers);
        //     $ticketworker->update([
        //         'availability_id' => 1
        //     ]);
        // }
        // $worker->save();

        return redirect(route('tickets.index'))
        ->with('updated_ticket', $ticket->id);
    }

    public function reject(Request $request, Ticket $ticket, Worker $worker)
    {
        $ticket->status_id = 4;
        $ticket->save();
        $ticket->workers[0]->where('id', $ticket->workers[0]->id)->update([
            'availability_id' => 1
        ]);

        return redirect(route('tickets.index'))
        ->with('updated_ticket', $ticket->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ticket $ticket)
    {
        $ticket -> delete();
        return redirect(route('tickets.index')); 
    }
}
