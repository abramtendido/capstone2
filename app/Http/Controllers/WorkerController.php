<?php

namespace App\Http\Controllers;

use App\Worker;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Category;
use Str;

class WorkerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $category = Category::all();
        $workers = Worker::all()->where('category_id',$request->category);
        return view('workers.index')
        ->with('workers', $workers)
        ->with('category', $category);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Worker $worker)
    {
        $this->authorize('create', $worker);

        $categories = Category::all();
        return view('workers.create') -> with('categories', $categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Worker $worker)
    {
        // $this->authorize('create', $worker);
        //Flow
            // 1. validate each field
        $request->validate([
            'name'=> 'required|string',
            'price'=> 'required|numeric',
            'category'=> 'required',
            'introduction'=> 'required|string',
            'serial_number'=> 'required|string',
            'image'=> 'required|image|max:10000' //kb
            
        ]);
            // 2. save uploaded image
        $file = $request->file('image');
        $file_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $file_extension = $file->extension();
        $random_chars = Str::random(8);

        $new_file_name = date('Y-m-d-H-i-s'). "_" . $random_chars . "_" . $file_name . "." . $file_extension;

        $filepath = $file->storeAs('images',$new_file_name, 'public');
        // dd($new_file_name);
            // 3. get input values from form
        $name = $request->input('name');
        $price = $request->input('price');
        $category_id = $request->input('category');
        $introduction = $request->input('introduction');
        $serial_number = $request->input('serial_number');
        $image = $filepath;
            // 4. save the details to database
        $worker = new worker;
        $worker->name = $name;
        $worker->price = $price;
        $worker->introduction = $introduction;
        $worker->serial_number = $serial_number;
        $worker->image = $image;
        $worker->category_id = $category_id;
        $worker->save();
            // 5. redirect to worker created
        return redirect( route('workers.show', ['worker' => $worker->id]));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Worker  $worker
     * @return \Illuminate\Http\Response
     */
    public function show(Worker $worker)
    {
     return view('workers.show')->with('worker', $worker);
     // $category = Category::all();
     // $worker = Worker::all();
     // return view('workers.show')
     // ->with('worker', $worker)
     // ->with('category', $category);
 }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Worker  $worker
     * @return \Illuminate\Http\Response
     */
    public function edit(Worker $worker)
    {
        // $this->authorize('update', $worker);

        $categories = Category::all();
        return view('workers.edit')
        ->with('worker', $worker)
        ->with('categories', $categories);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Worker  $worker
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Worker $worker)
    {
        // $this->authorize('update', $worker);
        //Flow
            // 1. validate each field
        $request->validate([
            'name'=> 'required|string',
            'price'=> 'required|numeric',
            'category'=> 'required',
            'introduction'=> 'required|string',
            'serial_number'=> 'required|string'
            
        ]);
            // 2. save uploaded image
        if (
            !$request->hasFile('image') && 
            $worker->name == $request->input('name') &&
            $worker->introduction == $request->input('introduction') &&
            $worker->serial_number == $request->input('serial_number') &&
            $worker->price == $request->input('price') &&
            $worker->category_id == $request->input('category')
        ) {
            $request->session()->flash('update_failed','Whoops! Something went wrong');
        }
        else{
            if($request->hasFile('image'))
            {
                $request->validate(
                    ['image' => 'required|image|max:10000']
                );

                // 2. save uploaded image
                $file = $request->file('image');
                $file_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $file_extension = $file->extension();
                $random_chars = Str::random(8);

                $new_file_name = date('Y-m-d-H-i-s'). "_" . $random_chars . "_" . $file_name . "." . $file_extension;

                $filepath = $file->storeAs('images',$new_file_name, 'public');

                // set new image path as new image value of worker
                $worker->image = $filepath;
            }

            $worker->name = $request->input('name');
            $worker->price = $request->input('price');
            $worker->category_id = $request->input('category');
            $worker->introduction = $request->input('introduction');
            $worker->serial_number = $request->input('serial_number');
            $worker->save();

            $request->session()->flash('update_success', 'Updated Worker!');
        }

        return redirect( route('workers.edit', ['worker' => $worker->id]));   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Worker  $worker
     * @return \Illuminate\Http\Response
     */
    public function destroy(Worker $worker)
    {
        // $this->authorize('delete', $worker);
        $worker->delete();
        // $request->session()->flash('destroy_success', 'Destroyed!');
        // return redirect(view('workers.index'));
        // return redirect(route('worker.index')) 
        // ->with('destroy_message', 'Worker Deleted');
        return redirect()->back()
        ->with('destroy_message', 'Worker Deleted');
    }

}
