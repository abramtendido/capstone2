<?php

namespace App\Http\Controllers;

use App\Ticket_Product;
use Illuminate\Http\Request;
use Session;

class TicketProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ticket_Product  $ticket_Product
     * @return \Illuminate\Http\Response
     */
    public function show(Ticket_Product $ticket_Product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ticket_Product  $ticket_Product
     * @return \Illuminate\Http\Response
     */
    public function edit(Ticket_Product $ticket_Product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ticket_Product  $ticket_Product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ticket_Product $ticket_Product)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ticket_Product  $ticket_Product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ticket_Product $ticket_Product)
    {
        //
    }
}
