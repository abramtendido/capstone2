<?php

namespace App\Http\Controllers;

use App\Availabilty;
use Illuminate\Http\Request;

class AvailabiltyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Availabilty  $availabilty
     * @return \Illuminate\Http\Response
     */
    public function show(Availabilty $availabilty)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Availabilty  $availabilty
     * @return \Illuminate\Http\Response
     */
    public function edit(Availabilty $availabilty)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Availabilty  $availabilty
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Availabilty $availabilty)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Availabilty  $availabilty
     * @return \Illuminate\Http\Response
     */
    public function destroy(Availabilty $availabilty)
    {
        //
    }
}
