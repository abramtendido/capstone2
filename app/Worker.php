<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Worker extends Model
{
    use SoftDeletes;
    protected $fillable = ['availability_id'];
	public function category()
    {
        return $this->belongsTo('App\Category');
    }
}
